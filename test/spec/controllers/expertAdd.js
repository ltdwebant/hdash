'use strict';

describe('Controller: ExpertaddCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var ExpertaddCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ExpertaddCtrl = $controller('ExpertaddCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
