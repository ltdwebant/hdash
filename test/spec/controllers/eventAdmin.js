'use strict';

describe('Controller: EventadminCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var EventadminCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EventadminCtrl = $controller('EventadminCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
