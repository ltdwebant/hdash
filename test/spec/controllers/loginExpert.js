'use strict';

describe('Controller: LoginexpertCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var LoginexpertCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LoginexpertCtrl = $controller('LoginexpertCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
