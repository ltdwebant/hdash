'use strict';

describe('Controller: ManagereditCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var ManagereditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ManagereditCtrl = $controller('ManagereditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
