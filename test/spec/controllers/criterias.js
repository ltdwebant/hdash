'use strict';

describe('Controller: CriteriasCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var CriteriasCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CriteriasCtrl = $controller('CriteriasCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
