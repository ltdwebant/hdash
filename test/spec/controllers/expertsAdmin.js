'use strict';

describe('Controller: ExpertsadminCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var ExpertsadminCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ExpertsadminCtrl = $controller('ExpertsadminCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
