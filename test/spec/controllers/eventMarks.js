'use strict';

describe('Controller: EventmarksCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var EventmarksCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EventmarksCtrl = $controller('EventmarksCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
