'use strict';

describe('Controller: ProjCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var ProjCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ProjCtrl = $controller('ProjCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
