'use strict';

describe('Controller: ExpertsmanagerCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var ExpertsmanagerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ExpertsmanagerCtrl = $controller('ExpertsmanagerCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
