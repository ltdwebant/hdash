'use strict';

describe('Controller: CriteriaeditCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var CriteriaeditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CriteriaeditCtrl = $controller('CriteriaeditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
