'use strict';

describe('Controller: EventaddCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var EventaddCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EventaddCtrl = $controller('EventaddCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
