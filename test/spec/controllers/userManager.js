'use strict';

describe('Controller: UsermanagerCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var UsermanagerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UsermanagerCtrl = $controller('UsermanagerCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
