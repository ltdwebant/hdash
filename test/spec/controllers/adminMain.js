'use strict';

describe('Controller: AdminmainCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var AdminmainCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminmainCtrl = $controller('AdminmainCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
