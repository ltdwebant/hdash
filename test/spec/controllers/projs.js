'use strict';

describe('Controller: ProjsCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var ProjsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ProjsCtrl = $controller('ProjsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
