'use strict';

describe('Controller: UseradminCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var UseradminCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UseradminCtrl = $controller('UseradminCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
