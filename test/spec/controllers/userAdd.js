'use strict';

describe('Controller: UseraddCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var UseraddCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UseraddCtrl = $controller('UseraddCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
