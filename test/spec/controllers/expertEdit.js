'use strict';

describe('Controller: ExperteditCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var ExperteditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ExperteditCtrl = $controller('ExperteditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
