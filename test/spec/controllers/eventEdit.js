'use strict';

describe('Controller: EventeditCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var EventeditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EventeditCtrl = $controller('EventeditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
