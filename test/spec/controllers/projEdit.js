'use strict';

describe('Controller: ProjeditCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var ProjeditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ProjeditCtrl = $controller('ProjeditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
