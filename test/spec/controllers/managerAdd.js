'use strict';

describe('Controller: ManageraddCtrl', function () {

  // load the controller's module
  beforeEach(module('hdashApp'));

  var ManageraddCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ManageraddCtrl = $controller('ManageraddCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
