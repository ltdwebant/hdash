'use strict';

describe('Service: Proj', function () {

  // load the service's module
  beforeEach(module('HdashApp'));

  // instantiate service
  var Proj;
  beforeEach(inject(function (_Proj_) {
    Proj = _Proj_;
  }));

  it('should do something', function () {
    expect(!!Proj).toBe(true);
  });

});
