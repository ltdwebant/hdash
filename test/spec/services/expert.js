'use strict';

describe('Service: Expert', function () {

  // load the service's module
  beforeEach(module('HdashApp'));

  // instantiate service
  var Expert;
  beforeEach(inject(function (_Expert_) {
    Expert = _Expert_;
  }));

  it('should do something', function () {
    expect(!!Expert).toBe(true);
  });

});
