'use strict';

describe('Service: Criteria', function () {

  // load the service's module
  beforeEach(module('HdashApp'));

  // instantiate service
  var Criteria;
  beforeEach(inject(function (_Criteria_) {
    Criteria = _Criteria_;
  }));

  it('should do something', function () {
    expect(!!Criteria).toBe(true);
  });

});
