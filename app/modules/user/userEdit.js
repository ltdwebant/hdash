'use strict';

angular.module('hdashApp')
  .controller('UsereditCtrl', function ($scope, $stateParams, Event, User, UtilF) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
        var id = $stateParams.id;
        if(id){
            User.VIEW(
                {id:id},
                function(data){
                    $scope.user = data;

                    Event.VIEW(
                        {id:data.active_event},
                        function (data) {
                            $scope.events = data;
                            $scope.user.event_id = data.active_event;
                        },
                        function (data) {
                            UtilF.catchConnectionError(data);
                        }
                    );

                },
                function(data){
                    UtilF.catchConnectionError(data);
                }
            )
        }
        $scope.save = function(){
            User.UPDATE(
                $scope.user,
                function(data){
                    UtilF.addAlert("Успешно сохранено", "success");
                },
                function(data){
                    UtilF.catchConnectionError(data);
                }
            )
        }
  });
